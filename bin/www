#!/usr/bin/env node

/**
 * Module dependencies.
 */

var fs = require('fs');
var app = require('../app');
var debug = require('debug')('chatgroup:server');
var https = require('https');
var http = require('http');


/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.PORT || '443');
app.set('port', port);

/**
 * Certificate
 */

var privateKey = fs.readFileSync('/etc/letsencrypt/live/solis.obsidiansoft.io/privkey.pem');
var certificate = fs.readFileSync('/etc/letsencrypt/live/solis.obsidiansoft.io/fullchain.pem');

var credentials = {
  key: privateKey,
  cert: certificate
};

/**
 * Create HTTP server.
 */

var server = https.createServer(credentials, app);
//var server = http.createServer(app);

/**
 * Socket.io
 */

app.io.attach(server);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port, () => console.log(`listening on port ${port}`));
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string' ?
    'Pipe ' + port :
    'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string' ?
    'pipe ' + addr :
    'port ' + addr.port;
  debug('Listening on ' + bind);
}