var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('client-sessions');
var compression = require('compression');
var helmet = require('helmet');

moment = require('moment');
formidable = require('formidable');
firebase = require('firebase');
admin = require('firebase-admin');
Auth = require('./lib/auth');

var serviceAccount = require("./chat-group-839f6-firebase-adminsdk-ide94-8dd9302e88.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://chat-group-839f6.firebaseio.com"
});

firebase.initializeApp({
  apiKey: "AIzaSyAiZTQkU2VuCIUb8zifevEFZeBisnanabk",
  authDomain: "chat-group-839f6.firebaseapp.com",
  databaseURL: "https://chat-group-839f6.firebaseio.com",
  projectId: "chat-group-839f6",
  storageBucket: "chat-group-839f6.appspot.com",
  messagingSenderId: "114402935790"
});

// db's references
var db = admin.database();
channels_db = db.ref("channels");


app = express();
app.use(helmet());

// Socket.io
app.io = require('socket.io')();

// socket.io events
app.io.on('connection', function (socket) {
  console.log(`new user connected`);
  socket.on('new message', function (channel) {
    console.log('new message to channel: ' + channel);
    app.io.emit('chat message', channel);
  });
  socket.on('new notification', function (notification) {
    console.log('new mention by user: ' + notification.user_send + ' to: ' + notification.user_recieved);
    app.io.emit('mention', notification);
  });
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(compression()); //Compress all routes
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(__dirname, {
  dotfiles: 'allow'
}));

//assets bower
app.use('/assets', [
  express.static(__dirname + '/node_modules/'),
]);

app.use(session({
  cookieName: 'session',
  secret: 'chat_app_session',
  duration: 30 * 60 * 1000,
  activeDuration: 5 * 60 * 1000,
}));

app.use(function (req, res, next) {
  if (req.session && req.session.user) {
    admin.auth().getUserByEmail(req.session.user.email)
      .then(function (user) {
        req.user = user;
        req.session.user = user;
        res.locals.user = user;
        next();
      })
      .catch(function (error) {
        console.log("Error fetching user data:", error);
      });
  } else {
    next();
  }
});


var indexRouter = require('./routes/index');
var apiRouter = require('./routes/api');

app.use('/', indexRouter);
app.use('/api', apiRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;