var socket = io();

var user_id;

$("#msg").on("keydown", function search(e) {
    if (e.keyCode == 13) {
        sendMessage();
    }
});


/**
 * Get channels by user_id
 *
 * @param {*} user_id
 */
function getChannelsByUser(user_id) {
    $.ajax({
        type: "GET",
        url: `api/channel_by_user/${user_id}`,
        success: function (res) {
            $('.list-channels').empty();
            $.each(res, (indx, val) => {
                $('.list-channels').append(
                    $('<li>').text(res[indx].name)
                    .addClass('list-group-item')
                    .attr('id', res[indx].name)
                );
            });
            $(".list-channels .list-group-item").click(function (e) {
                $("#btn_newuser").removeAttr("disabled");
                $("#msg").removeAttr("disabled");
                $("#btn_sendmsg").removeAttr("disabled");
                $(".list-channels .list-group-item").removeClass("active");
                $(e.target).addClass("active");
                $('#current_id_channel').val($(e.target).attr('id'));
                getUsersByChannel($(e.target).attr('id'));
                getChatsByChannel($(e.target).attr('id'));
            });
        },
        error: function (msg) {
            Swal.fire("Ups!", msg.responseJSON.error, "error");
        }
    });
}

/**
 * Get user by channel id
 *
 * @param {*} id_channel
 */
function getUsersByChannel(id_channel) {
    $.ajax({
        type: "GET",
        url: `api/users_by_channel/${id_channel}`,
        success: function (res) {
            $('.list-users').empty();
            $.each(res.users, (indx, val) => {
                $('.list-users').append(
                    $('<li>').text(val)
                    .addClass('list-group-item')
                    .attr('id', val)
                );
            });
            $(".list-users .list-group-item").click(function (e) {
                $(".list-users .list-group-item").removeClass("active");
                $(e.target).addClass("active");
            });
        },
        error: function (msg) {
            Swal.fire("Ups!", msg.responseJSON.error, "error");
        }
    });
}

/**
 * Get chats by channel id
 *
 * @param {*} id_channel
 */
function getChatsByChannel(id_channel) {
    $.ajax({
        type: "GET",
        url: `api/chats_by_channel/${id_channel}`,
        success: function (res) {
            $('.msg_history').empty();
            $.each(res.chat, (indx, val) => {
                if (val.name != user_id)
                    $('.msg_history').append('<div class="incoming_msg">' +
                        '<div class="received_msg">' +
                        '<div class="received_withd_msg">' +
                        '<br>' + val.name + ':</br><p>' + val.msg + '</p>' +
                        '<span class="time_date">' + val.time + '</span>' +
                        '</div>' +
                        '</div>' +
                        '</div>');
                else $('.msg_history').append('<div class="outgoing_msg">' +
                    '<div class="sent_msg">' +
                    '<br>' + val.name + ':</br><p>' + val.msg + '</p>' +
                    '<span class="time_date">' + val.time + '</span>' +
                    '</div>' +
                    '</div>');
            });
            $('.msg_history').animate({
                scrollTop: $('.msg_history').prop('scrollHeight')
            }, 1000);
        },
        error: function (msg) {
            Swal.fire("Ups!", msg.responseJSON.error, "error");
        }
    });
}

/**
 * Post message to the current selected channel
 *
 */
function sendMessage() {
    var data = new FormData();
    data.append('name', user_id);
    data.append('msg', $('#msg').val());

    $.ajax({
        type: "GET",
        url: "api/all_users",
        success: function (res) {
            res = res.map((val) => val.displayName);
            var msg = $('#msg').val();
            var regex = /(\@.*?\.)/g;
            var users_in_msg = msg.match(regex);
            if (users_in_msg) {
                users_in_msg = users_in_msg.map((val) => val.replace(/\@|\./g, ''));
                users_in_msg = _.intersection(res, users_in_msg);

                for (i in users_in_msg) {
                    var notification = {
                        channel: $('#current_id_channel').val(),
                        user_send: user_id,
                        user_recieved: users_in_msg[i]
                    }
                    socket.emit('new notification', notification);
                }
            }
        },
        error: function (msg) {
            Swal.fire("Ups!", msg.responseJSON.error, "error");
        }
    });

    $.ajax({
        type: "POST",
        url: `api/new_message/${$('#current_id_channel').val()}`,
        data: data,
        processData: false,
        contentType: false,
        success: function (data) {
            $('#msg').val('');
            socket.emit('new message', $('#current_id_channel').val());
        },
        error: function (msg) {
            Swal.fire("Ups!", msg.responseJSON.error, "error");
        }
    });
}

socket.on('mention', function (notification) {
    if (user_id == notification.user_recieved)
        Swal.fire(`New mention in channel (${notification.channel}) by ${notification.user_send} to ${notification.user_recieved}`);
});

socket.on('chat message', function (channel) {
    getChatsByChannel(channel);
});

/**
 * Create new channel
 *
 */
function setChannel() {
    var $content = $(
        '<div class="form-group"><label for="channel">Channel</label>' +
        '<input type="text" class="form-control" id="channel" placeholder="channel name..">' +
        '</div>'
    );

    let dialog = BootstrapDialog.show({
        message: $content,
        title: 'Create new chat channel: ',
        buttons: [{
            icon: 'far fa-save',
            label: ' Agregar',
            cssClass: 'btn-primary',
            action: function (dialog) {
                if ($('#channel').val() != "") {
                    var data = new FormData();
                    data.append('name', $('#channel').val());
                    data.append('users', JSON.stringify([user_id]));
                    $.ajax({
                        type: "POST",
                        url: "api/new_channel",
                        data: data,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            let inv_code = '/invitation_code/' + encodeURIComponent($('#channel').val());
                            BootstrapDialog.show({
                                title: 'Invitation Code',
                                message: $('<b>You can share this invitation code: </b><p>' +
                                    `<i class="fas fa-share-square"></i> ${inv_code}</p>`)
                            });
                            dialog.close();
                            getChannelsByUser(user_id);
                        },
                        error: function (msg) {
                            Swal.fire("Ups!", msg.responseJSON.error, "error");
                        }
                    });
                }
            }
        }]
    });
}

/**
 * Add new user to the current channel
 *
 */
function setNewUser() {
    var $content = $(
        '<div class="form-group"><label for="channel">Set new user to the channel: </label>' +
        '<select id="user" class="selectpicker"></select>' +
        '</div>'
    );

    var dialog = BootstrapDialog.show({
        message: $content,
        title: 'Add user to this channel: ',
        buttons: [{
            icon: 'far fa-save',
            label: ' Agregar',
            cssClass: 'btn-primary',
            action: function (dialog) {
                var data = new FormData();
                data.append('user', $('#user').val());
                $.ajax({
                    type: "POST",
                    url: `api/channel/${$('#current_id_channel').val()}`,
                    data: data,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        dialog.close();
                        getUsersByChannel($('#current_id_channel').val());
                    },
                    error: function (msg) {
                        Swal.fire("Ups!", msg.responseJSON.error, "error");
                    }
                });

            }
        }],
        onshown: function (diagloRef) {
            $.ajax({
                type: "GET",
                url: "api/all_users",
                success: function (res) {
                    res = res.map((val) => val.displayName);
                    $.ajax({
                        type: "GET",
                        url: `api/users_by_channel/${$('#current_id_channel').val()}`,
                        success: function (users_channel) {
                            res = _.difference(res, Object.values(users_channel.users));
                            $.each(res, (index, val) => {
                                $('#user').append('<option value="' + val + '">' + val +
                                    '</option>');
                            });
                            $.fn.selectpicker.Constructor.BootstrapVersion = '4';
                            $('#user').selectpicker({
                                liveSearch: true,
                                liveSearchNormalize: true,
                                showTick: true,
                                size: 6,
                                virtualScroll: true,
                                dropupAuto: false
                            });
                        },
                        error: function (msg) {
                            Swal.fire("Ups!", msg.responseJSON.error, "error");
                        }
                    });
                },
                error: function (msg) {
                    Swal.fire("Ups!", msg.responseJSON.error, "error");
                }
            });
        },
    });
}

/**
 * Adds new user to a channel based on an invitation link
 *
 */
function add2Channel() {
    var $content = $(
        '<div class="form-group"><label for="inv_code">Add to created channel: </label>' +
        '<input type="text" class="form-control" id="inv_code" placeholder="Paste invitation code here..">' +
        '</div>'
    );

    let dialog = BootstrapDialog.show({
        message: $content,
        title: 'Add to new chat channel: ',
        buttons: [{
            icon: 'far fa-save',
            label: ' Agregar',
            cssClass: 'btn-primary',
            action: function (dialog) {
                if ($('#inv_code').val() != "") {
                    var data = new FormData();
                    data.append('user', user_id);
                    $.ajax({
                        type: "POST",
                        url: "api" + $('#inv_code').val(),
                        data: data,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            dialog.close();
                            getChannelsByUser(user_id);
                        },
                        error: function (msg) {
                            Swal.fire("Ups!", "Invalid invitation code!", "error");
                        }
                    });
                }
            }
        }]
    });
}