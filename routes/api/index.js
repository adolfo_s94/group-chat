var express = require('express');
var router = express.Router();

router.get('/all_channels', function (req, res, next) {
    channels_db.once("value", (channels, error) => {
        if (error) res.status(500).send({
            error: error
        });
        else res.send(channels);
    });
});


router.get('/channel_by_user/:id', function (req, res, next) {
    var id = req.params.id;
    channels_db.once("value", (channels, error) => {
        channels = channels.val();
        channels = Array.from(Object.keys(channels), k => channels[k]);
        var channels = channels.filter(function (el) {
            el.users = Array.from(Object.keys(el.users), k => el.users[k])
            return el.users.includes(id)
        });
        if (error) res.status(500).send({
            error: error
        });
        else res.send(channels);
    });
});


router.get('/all_users', function (req, res, next) {
    admin.auth().listUsers(1000)
        .then(function (listUsersResult) {
            res.status(200).send(listUsersResult.users);
        })
        .catch(function (error) {
            res.status(500).send({
                error: error
            });
        });
});

router.get('/users_by_channel/:id', function (req, res, next) {
    var id = req.params.id;
    channels_db.once("value", (channels, error) => {
        channels = channels.val();
        channels = Array.from(Object.keys(channels), k => channels[k]);
        var channels = channels.filter(function (el) {
            return el.name == id
        });
        if (error) res.status(500).send({
            error: error
        });
        else {
            res.send(channels[0])
        };
    });
});

router.get('/chats_by_channel/:id', function (req, res, next) {
    var id = req.params.id;
    channels_db.once("value", (channels, error) => {
        channels = channels.val();
        channels = Array.from(Object.keys(channels), k => channels[k]);
        var channels = channels.filter(function (el) {
            return el.name == id
        });
        if (error) res.status(500).send({
            error: error
        });
        else {
            res.send(channels[0]);
        };
    });
});


router.post('/new_channel', (req, res, next) => {
    var form = new formidable.IncomingForm()
    form.mulitples = true;
    form.parse(req, function (err, fields, files) {
        fields.users = JSON.parse(fields.users)
        channels_db.push(fields).then(function (newChannel) {
            res.status(200).send(newChannel.key);
        }).catch(function (error) {
            res.status(500).send({
                error: error
            });
        });
    });
});

router.post('/channel/:id', (req, res, next) => {
    var id = req.params.id;
    var form = new formidable.IncomingForm()
    form.mulitples = true;
    form.parse(req, function (err, fields, files) {
        channels_db.orderByChild("name").equalTo(id).once("child_added", function (snapshot) {
            channels_db.child(snapshot.key).child('users').push(fields.user, (error) => {
                if (error) res.status(500).send({
                    error: error
                });
                else res.send(fields);
            });
        });
    });
});

router.post('/invitation_code/:id', (req, res, next) => {
    var id = req.params.id;
    var form = new formidable.IncomingForm()
    form.mulitples = true;
    form.parse(req, function (err, fields, files) {
        channels_db.orderByChild("name").equalTo(id).once("child_added", function (snapshot) {
            channels_db.child(snapshot.key).child('users').push(fields.user, (error) => {
                if (error) res.status(500).send({
                    error: 'Invalid invitation code!'
                });
                else res.send(fields);
            });
        }).catch((error) => {
            if (error) res.status(500).send({
                error: 'Invalid invitation code!'
            });
        });
    });
});

router.post('/new_message/:id', (req, res, next) => {
    var id = req.params.id;
    var form = new formidable.IncomingForm()
    form.mulitples = true;
    form.parse(req, function (err, fields, files) {
        channels_db.orderByChild("name").equalTo(id).once("child_added", function (snapshot) {
            fields.time = moment().format('HH:mm | YYYY/MM/DD').toString();
            channels_db.child(snapshot.key).child('chat').push(fields, (error) => {
                if (err) res.status(500).send({
                    error: error
                });
                else {
                    res.send(fields)
                };
            });
        });
    });
});


module.exports = router;