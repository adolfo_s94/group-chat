var express = require('express');
var router = express.Router();


router.get('/sign_out', (req, res, next) => {
  var form = new formidable.IncomingForm()
  form.parse(req, function (err, fields, files) {
    firebase.auth().signOut().then(function () {
      req.session.reset();
      res.redirect('/')
    }).catch(function (error) {
      console.log(error)
    });
  });
});

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Chat Group'
  });
});

router.post('/', (req, res, next) => {
  var form = new formidable.IncomingForm()
  form.parse(req, function (err, fields, files) {
    firebase.auth().signInWithEmailAndPassword(fields.email, fields.password).then((info) => {
      req.session.user = info.user;
      res.locals.user = req.session.user;
      res.status(200).send(req.session.user.displayName);
    }).catch((error) => {
      req.session.reset();
      res.status(500).send({
        error: 'Usuario y/o contraseña inválidos..'
      });
    });
  });
});

router.get('/register', function (req, res, next) {
  res.render('register', {
    title: 'Register User for Chat Group'
  });
});

router.get('/recover', function (req, res, next) {
  res.render('recover', {
    title: 'Recover user password'
  });
});

router.post("/recover", (req, res, next) => {
  var form = new formidable.IncomingForm();
  form.mulitples = true;
  form.parse(req, function (err, fields, files) {
    firebase
      .auth().sendPasswordResetEmail(fields.email).then(function () {
        res.status(200).send(fields.email);
      }).catch(function (error) {
        res.status(500).send({
          error: error
        });
      });
  });
});

router.post("/register", (req, res, next) => {
  var form = new formidable.IncomingForm();
  form.mulitples = true;
  form.parse(req, function (err, fields, files) {
    admin
      .auth()
      .createUser({
        email: fields.email,
        emailVerified: false,
        password: fields.password,
        displayName: fields.nombre_completo,
        disabled: false,
      }).then(function (userRecord) {
        res.status(200).send(userRecord.displayName);
      }).catch(function (error) {
        res.status(500).send({
          error: error
        });
      });
  });
});

router.get('/home', Auth.requireLogin, function (req, res, next) {
  res.render('home', {
    title: 'Home - Chat Group'
  });
});

module.exports = router;