# Group chat

Group chat based on NodeJS, Firebase and Socket.io

## Functions

- Register Users
- Login Users
- Create new chat channels
- Invite new users to a current chat channel using an invitation code.
- Real time chat
- Notifications on mentions